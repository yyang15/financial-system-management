package unl.cse;

public class PrivateInvestment extends Investment {

	private double TotalValue;
	private double value;

	public PrivateInvestment() {

	}

	public PrivateInvestment(String assetsCode, String label, String type,
			double quarterDividend, double baseRateReturn, double omega,
			double TotalValue) {
		super(assetsCode, label, type, quarterDividend, baseRateReturn, omega);
		this.TotalValue = TotalValue;
	}

	/*
	 * copy constractor
	 */
	public PrivateInvestment(PrivateInvestment a) {
		this(a.getAssetsCode(), a.getLabel(), a.getType(), a
				.getQuarterlyDividend(), a.getBaseRateOfReturn(), a.getOmega(),
				a.getTotalValue());

	}

	/*
	 * these compute-## method is for calculating.
	 */
//	public double compute_value() {
//		return this.TotalValue * this.temp_value;
//		// return (double) Math.round(this.TotalValue * this.temp_value / 100.0)
//		// * 10000 / 10000.0;
//	}
//
//	public double compute_risk() {
//		// TODO Auto-generated method stub
//		return this.omega + 0.25;
//
//	}
//
//	@Override
//	public double compute_returnRate() {
//		// TODO Auto-generated method stub
//		// return/value
//		return (this.baseRateOfReturn * this.TotalValue + 4 * this.quarterlyDividend)
//				* this.temp_value
//				/ 100
//				/ (this.TotalValue * this.temp_value / 100.0);
		// return (double) Math
		// .round((this.baseRateOfReturn * this.TotalValue + 4 *
		// this.quarterlyDividend)
		// * this.temp_value
		// / 100
		// / (this.TotalValue * this.temp_value / 100.0)) * 100 / 100;

//	}

//	@Override
//	public double compute_return() {
//		// TODO Auto-generated method stub
//		return (this.baseRateOfReturn * this.TotalValue + 4 * this.quarterlyDividend)
//				* this.temp_value;
//	}

	public void print() {
		System.out.println(this.assetsCode + "," + this.label + "," + this.type
				+ "," + this.quarterlyDividend + "," + this.baseRateOfReturn
				+ "," + this.TotalValue + "," + this.omega + ","
				+ this.temp_value);
	}

	public double getValue() {
		return this.TotalValue * this.temp_value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public double getReturnRate() {
		return (this.baseRateOfReturn * this.TotalValue + 4 * this.quarterlyDividend)
				* this.temp_value
				/ 100
				/ (this.TotalValue * this.temp_value / 100.0);
	}

	public void setReturnRate(double returnRate) {
		this.returnRate = returnRate;
	}

	public double getRisk() {
		return this.omega + 0.25;
	}

	public void setRisk(double risk) {
		this.risk = risk;
	}

	public double getAnnualReturn() {
		return (this.baseRateOfReturn * this.TotalValue + 4 * this.quarterlyDividend)
				* this.temp_value;
	}

	public void setAnnualReturn(double annualReturn) {
		this.annualReturn = annualReturn;
	}

	public double getTotalValue() {
		return TotalValue;
	}

	public void setTotalValue(double totalValue) {
		TotalValue = totalValue;
	}
	
	/*
	 * use this method to print the privateInvestment object to the XML style
	 */
	/*
	 * public String toXML() { return super.toXML() + "  <quarterlyDividend>" +
	 * this.quarterlyDividend + "</quarterlyDividend>\n  <baseRateOfReturn>" +
	 * this.baseRateOfReturn + "</baseRateOfReturn>\n  <omega>" + this.omega +
	 * "</omega>\n  <totalValue>" + this.totalValue +
	 * "</totalValue>\n</asset>\n\n"; }
	 */
	/*
	 * use this method to print the privateInvestment object to the Json style
	 */
	/*
	 * public String toJson() { return super.toJson() +
	 * "\n  \"quarterlyDividend\": " + this.quarterlyDividend +
	 * ",\n  \"baseRateOfReturn\": " + this.baseRateOfReturn +
	 * ",\n  \"omega\": " + this.omega + ",\n  \"totalValue\": " +
	 * this.totalValue + "\n}";
	 * 
	 * }
	 */

}
