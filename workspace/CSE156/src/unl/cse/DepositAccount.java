package unl.cse;

public class DepositAccount extends Asset {

	private double apr;
	@SuppressWarnings("unused")
	private double APY;

	public DepositAccount(String assetsCode, String label, String type,
			double apr) {
		super(assetsCode, label, type);
		this.apr = apr;
	}

	/*
	 * copy constractor
	 */
	public DepositAccount(DepositAccount old) {
		this(old.getAssetsCode(), old.getLabel(), old.getType(), old.getApr());
	}

	public DepositAccount() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * these compute-## method is for calculating.
	 */
	// public double compute_value()
	// {
	// return this.temp_value;
	// // return (double)Math.round(this.temp_value)*100/100;
	// }
	// @Override
	// public double compute_risk() {
	// // TODO Auto-generated method stub
	// return 0.00;
	// }

	@Override
	// public double compute_returnRate() {
	// // TODO Auto-generated method stub
	// return this.temp_value*this.getAPY()/this.temp_value;
	//
	// }
	// @Override
	// public double compute_return() {
	// // TODO Auto-generated method stub
	// return this.temp_value*this.getAPY();
	// }
	// @Override
	public void print() {
		System.out.println(this.assetsCode + "," + this.label + "," + this.type
				+ "," + this.apr + "," + this.temp_value);
	}

	public double getRisk() {
		return 0.00;
	}

	public void setRisk(double new_risk) {
		this.risk = new_risk;
	}

	public double getAnnualReturn() {
		return this.temp_value * this.getAPY();
	}

	public void setAnnualReturn(double annualReturn) {
		this.annualReturn = annualReturn;
	}

	public double getValue() {
		return this.temp_value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public void setAPY() {
		APY = Math.pow(Math.E, this.getApr()) - 1;
	}

	public double getAPY() {
		return Math.pow(Math.E, this.getApr()) - 1;
	}

	public double getApr() {
		return apr;
	}

	public void setApr(double apr) {
		this.apr = apr;
	}

	public double getReturnRate() {
		return this.temp_value * this.getAPY() / this.temp_value;
	}

	public void setReturnRate(double returnRate) {
		this.returnRate = returnRate;
	}

	public void setAPY(double aPY) {
		APY = aPY;
	}

	/*
	 * use this method to print the DepositAccount object to the XML style
	 */
	/*
	 * public String toXML() { return super.toXML() + "  <apr>" + this.apr +
	 * "</apr>\n</asset>\n\n"; } /* use this method to print the DepositAccount
	 * object to the Json style
	 */
	/*
	 * public String toJson(){ return
	 * super.toJson()+"\n  \"apr\": "+this.apr+"\n}";
	 * 
	 * }
	 */

}