package unl.cse;

public abstract class Investment extends Asset{
	protected double quarterlyDividend;
	protected double baseRateOfReturn;
	protected double omega;
	public Investment(){
		
	}
	public Investment(String assetsCode, String label, String type,double quarterDividend,double baseRateReturn,double omega) {
		super(assetsCode, label, type);
		
		// TODO Auto-generated constructor stub
		this.quarterlyDividend = quarterDividend;
		this.baseRateOfReturn = baseRateReturn;
		this.omega = omega;
		
	}
	public double getQuarterlyDividend() {
		return quarterlyDividend;
	}
	public void setQuarterlyDividend(double quarterlyDividend) {
		this.quarterlyDividend = quarterlyDividend;
	}
	public double getBaseRateOfReturn() {
		return baseRateOfReturn;
	}
	public void setBaseRateOfReturn(double baseRateOfReturn) {
		this.baseRateOfReturn = baseRateOfReturn;
	}
	public double getOmega() {
		return omega;
	}
	public void setOmega(double omega) {
		this.omega = omega;
	}

}
