package unl.cse;

import java.util.Comparator;
import java.util.Iterator;

public class SortedList<T> implements Iterable<T> {
	private Node<T> head = null;
	private Comparator<T> comparator = null;

	public SortedList(Comparator<T> comparator) {
		this.comparator = comparator;
	}

	public void add(T item) {
		Node<T> newNode = new Node<T>(item);
		// compare it and all elements in this list by using comparator, and
		// arrange them
		Node<T> currentNode = null;
		currentNode = head;
		Node<T> lastNode = null;
		if (head == null) {
			head = newNode;
		} else {
			if (comparator.compare(currentNode.getItem(), newNode.getItem()) > 0) {
				head = newNode;
				head.setNext(currentNode);
			} else {
				while (comparator.compare(currentNode.getItem(),
						newNode.getItem()) <= 0) {
					if (currentNode.hasNext()) {
						lastNode = currentNode;
						currentNode = currentNode.getNext();
					} else {
						currentNode.setNext(newNode);
						return;
					}
				}
				lastNode.setNext(newNode);
				newNode.setNext(currentNode);
			}

		}
	}

	public int size() {
		Node<T> currentNode = head;
		int counter = 0;
		while (currentNode != null) {
			currentNode = currentNode.getNext();
			counter++;
		}
		return counter;
	}

	private Node<T> getNodeAtIndex(int index) {
		if (index < 0 || index >= this.size()) {
			throw new IllegalArgumentException("index out of bounds");
		}
		Node<T> currentNode = head;
		for (int i = 0; i < index; i++) {
			currentNode = currentNode.getNext();
		}
		return currentNode;

	}

	public void remove(T item) {
		if (head == null) {
			return;
		} else if (head.getItem().equals(item)) {
			head = head.getNext();
		} else {
			Node<T> current = head;
			Node<T> previous = null;
			while (current.hasNext() && !current.getItem().equals(item)) {
				previous = current;
				current = current.getNext();
			}
			if (current.getItem().equals(item))
				previous.setNext(current.getNext());
		}
	}

	public int getIndex(T item) {
		int counter = 0;
		Node<T> curr = head;
		while (curr.hasNext()) {
			if (comparator.compare(item, curr.getItem()) == 0) {
				return counter;
			}
			curr = curr.getNext();
			counter++;
		}
		return -1;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			Node<T> tem = head;

			@Override
			public boolean hasNext() {
				return tem != null;
			}

			@Override
			public T next() {
				if (tem == null) {
					throw new java.util.NoSuchElementException();
				}
				T item = tem.getItem();
				tem = tem.getNext();
				return item;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("not implemented");
			}
		};
	}

}