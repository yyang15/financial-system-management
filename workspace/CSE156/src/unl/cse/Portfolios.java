package unl.cse;

import java.util.List;

public class Portfolios {

	private String portfolioCode;
	private Person owner;
	private Broker broker;
	private Person beneficiary;
	private List<Asset> assetList;
	private double fees;
	private double comissions;
	private double weightedRisk;
	private double returnValue;
	private double totalValue;

	public Portfolios() {

	}

	public Portfolios(String portfolioCode, Person owner, Broker broker,
			Person beneficiary, List<Asset> assetList) {
		this.portfolioCode = portfolioCode;
		this.owner = owner;
		this.broker = broker;
		this.beneficiary = beneficiary;
		this.assetList = assetList;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	public Broker getBroker() {
		return broker;
	}

	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	public Person getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(Person beneficiary) {
		this.beneficiary = beneficiary;
	}

	//
	// public void print()
	// {
	// //System.out.printf("\n" + portfolioCode + ", " +
	// this.owner.getLastName() + ", " + this.broker.getLastName() + ", ");
	// /*
	// if(null != this.beneficiary)
	// System.out.print(this.beneficiary.getLastName() + ", ");
	//
	// if(null != this.assetList)
	// {
	// for(int i=0; i<this.assetList.size(); ++i)
	// this.assetList.get(i).print();
	// }
	// */
	// // System.out.println("fees=" + this.fees+ "code"+this.portfolioCode);
	// // System.out.println("\n--------------------\n");
	// }

	/*
	 * this method is for printing the summary reprot
	 */
	public void printSummaryReport() {

		System.out.printf(
				"\n%-12s%-22s%-22s%2s%10.2f%3s%12.2f%20.4f%3s%12.2f%3s%13.2f",
				this.portfolioCode,
				this.owner.getLastName() + "," + this.owner.getFirstName(),
				this.broker.getLastName() + "," + this.broker.getFirstName(),
				"$", this.getFees(), "$", this.getComissions(),
				this.getWeightedRisk(), "$", this.getReturnValue(), "$",
				this.getTotalValue());
	}

	/*
	 * this method is for printing the detail report
	 */
	public void printDetailReport() {
		System.out.printf("\nPortfolio  " + this.portfolioCode + "\n"
				+ "------------------------------------------\n");
		System.out.printf("%-18s%s\n", "Owner:", this.owner.getLastName() + ","
				+ this.owner.getFirstName());
		System.out.printf("%-18s%s\n", "Manager:", this.broker.getLastName()
				+ "," + this.broker.getFirstName());
		if (this.beneficiary == null) {
			System.out.printf("%-18s%s\n%s", "Beneficiary:", "None", "Assets");
		} else {
			System.out.printf(
					"%-18s%s\n%s",
					"Beneficiary:",
					this.beneficiary.getLastName() + ","
							+ this.beneficiary.getFirstName(), "Assets");
		}
		System.out.printf("\n%-12s%-21s%24s%14s%16s%17s", "Code", "Asset",
				"Return Rate", "Risk (omega)", "Annual Return", "Value");
		if (this.assetList == null) {
			System.out
					.printf("\n                                                        --------------------------------------------\n");
			System.out.printf("\n%45s%12s%14.4f%3s%13.2f%3s%14.2f\n", " ",
					"Totals", 0.0000, "$", 0.00, "$", 0.00);
		} else {
			for (int i = 0; i < this.assetList.size(); i++) {
				System.out.printf(
						"\n%-12s%-37s%7.2f%2s%13.2f%3s%13.2f%3s%14.2f",
						this.assetList.get(i).assetsCode,
						this.assetList.get(i).label, this.assetList.get(i)
								.getReturnRate() * 100, "%", this.assetList
								.get(i).getRisk(), "$", this.assetList.get(i)
								.getAnnualReturn(), "$", this.assetList.get(i)
								.getValue());

			}
			System.out
					.printf("\n                                                        --------------------------------------------");
			System.out.printf("\n%45s%12s%14.4f%3s%13.2f%3s%14.2f\n", " ",
					"Totals", this.weightedRisk, "$", this.returnValue, "$",
					this.totalValue);
		}

	}

	/*
	 * this method is for calculating
	 */
	// public void computation() {
	// double tem_value = 0.0;
	// if (null != this.assetList) {
	// for (int i = 0; i < this.assetList.size(); i++) {
	//
	// this.totalValue = this.totalValue
	// + this.assetList.get(i).compute_value();
	//
	// this.returnValue = this.returnValue
	// + this.assetList.get(i).compute_return();
	// tem_value = tem_value
	// + (this.assetList.get(i).compute_risk() * this.assetList
	// .get(i).compute_value());
	// }
	// } else {
	//
	// }
	// if (this.totalValue == 0) {
	// this.weightedRisk = 0.00;
	// } else {
	// this.weightedRisk = tem_value / this.totalValue;
	//
	// }
	// if (this.broker instanceof ExpertBroker) {
	// if (null == this.assetList) {
	// this.fees = 0.0;
	// this.comissions = 0.0;
	// } else {
	// this.fees = 10.0 * this.assetList.size();
	// this.comissions = this.returnValue * 0.05;
	//
	// }
	// } else if (this.broker instanceof JuniorBroker) {
	// if (null == this.assetList) {
	// this.fees = 0.0;
	// } else {
	// this.fees = 50.0 * this.assetList.size();
	// this.comissions = this.returnValue * 0.02;
	//
	// }
	// } else
	// fees = 0;
	// System.out.println("");
	//
	// }

	public String getPortfolioCode() {
		return portfolioCode;
	}

	public List<Asset> getAssetList() {
		return assetList;
	}

	public void setAssetList(List<Asset> assetList) {
		this.assetList = assetList;
	}

	public double getFees() {
		this.fees=0;
		if (this.broker instanceof ExpertBroker) {
			if (null == this.assetList) {
				this.fees = 0.0;
			} else {
				this.fees = 10.0 * this.assetList.size();

			}
		} else if (this.broker instanceof JuniorBroker) {
			if (null == this.assetList) {
				this.fees = 0.0;
			} else {
				this.fees = 50.0 * this.assetList.size();

			}
		} else
			fees = 0;
		return fees;
	}

	public void setFees(double fees) {
		this.fees = fees;
	}

	public double getComissions() {
		
		if (this.broker instanceof ExpertBroker) {
			if (null == this.assetList) {
				this.comissions = 0.0;
			} else {
				this.comissions = this.returnValue * 0.05;

			}
		} else if (this.broker instanceof JuniorBroker) {
			if (null == this.assetList) {
				this.comissions = 0;
			} else {
				this.comissions = this.returnValue * 0.02;

			}
		} else{
			
		}

		return this.comissions;
	}

	public void setComissions(double comissions) {
		this.comissions = comissions;
	}

	public double getWeightedRisk() {
		this.weightedRisk=0;
		double tem_value = 0.0;

		if (null != this.assetList) {
			for (int i = 0; i < this.assetList.size(); i++) {
				tem_value = tem_value
						+ (this.assetList.get(i).getRisk() * this.assetList
								.get(i).getValue());
			}
		} else {

		}
		if (this.totalValue == 0) {
			this.weightedRisk = 0.00;
		} else {
			this.weightedRisk = tem_value / this.totalValue;

		}
		return weightedRisk;
	}

	public void setWeightedRisk(double weightedRisk) {
		this.weightedRisk = weightedRisk;
	}

	public double getReturnValue() {
		this.returnValue=0;
		if (null != this.assetList) {
			for (int i = 0; i < this.assetList.size(); i++) {

				this.returnValue = this.returnValue
						+ this.assetList.get(i).getAnnualReturn();
			}
		} else {

		}
		return returnValue;
	}

	public void setReturnValue(double returnValue) {
		this.returnValue = returnValue;
	}

	public double getTotalValue() {
		this.totalValue=0;
		if (null != this.assetList) {
			for (int i = 0; i < this.assetList.size(); i++) {

				this.totalValue = this.totalValue
						+ this.assetList.get(i).getValue();
			}
		}
		return totalValue;
	}

	public void setPortfolioCode(String portfolioCode) {
		this.portfolioCode = portfolioCode;
	}

}
