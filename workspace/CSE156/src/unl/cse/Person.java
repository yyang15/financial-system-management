package unl.cse;

import java.util.List;

public class Person {
	protected List<String> EmailAddress;
	protected String brokerData;
	protected String personCode;
	protected String firstName;
	protected String lastName;
	protected String street;
	protected String city;
	protected String state;
	protected String country;
	protected String zepCode;
	protected String typeOfPerson;

	public Person() {
	}

	public Person(List<String> emailAddress, String brokerData, String personCode,
			String firstName, String lastName, String street, String city,
			String state, String country, String zepCode, String typeOfPerson) {
		this.EmailAddress = emailAddress;
		this.brokerData = brokerData;
		this.personCode = personCode;
		this.firstName = firstName;
		this.lastName = lastName;
		this.street = street;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zepCode = zepCode;
		this.typeOfPerson = typeOfPerson;
	}

	public Person(Person old_person) {
		this(old_person.getEmailAddress(), old_person.getBrokerData(),
				old_person.getPersonCode(), old_person.getFirstName(),
				old_person.getLastName(), old_person.getStreet(), old_person
						.getCity(), old_person.getState(), old_person
						.getCountry(), old_person.getZepCode(), old_person
						.getTypeOfPerson());
	}

	public Person(String personCode, String typeOfPerson, String name,
			String address, List<String> emailAddress) {
		this.typeOfPerson = typeOfPerson;
		this.personCode = personCode;
		String[] n = name.split(",");
		this.lastName = n[0];
		this.firstName = n[1];
		String a[] = address.split(",");
		this.street = a[0];
		this.city = a[1];
		this.state = a[2];
		this.zepCode = a[3];
		this.country = a[4];
		this.EmailAddress = emailAddress;
		this.brokerData = "";
	}

	public void print_all_details() {
		System.out.println(this.personCode + ", " + this.lastName + ", "
				+ this.firstName + "," + this.street + "," + this.city + ","
				+ this.state + "," + this.country + "," + this.zepCode + ","
				+ this.typeOfPerson);

	}

	public String getTypeOfPerson() {
		return typeOfPerson;
	}

	public void setTypeOfPerson(String typeOfPerson) {
		this.typeOfPerson = typeOfPerson;
	}

	public List<String> getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(List<String> emailAddress) {
		EmailAddress = emailAddress;
	}

	public String getBrokerData() {
		return brokerData;
	}

	public void setBrokerData(String brokerData) {
		this.brokerData = brokerData;
	}

	public String getPersonCode() {
		return personCode;
	}

	public void setPersonCode(String personCode) {
		this.personCode = personCode;
	}

	public String getZepCode() {
		return zepCode;
	}

	public void setZepCode(String zepCode) {
		this.zepCode = zepCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	/*
	 * use this method to print the person object to the XML style
	 */
	/*
	 * public String toXML() { String a = new String(); a =
	 * "<person> \n  <name>\n    <first>" + this.firstName +
	 * "</first>\n    <last>" + this.lastName +
	 * "</last>\n  </name>\n  <address>\n    <street>" + this.street +
	 * "</street>\n    <city>" + this.city + "</city>\n    <state>" + this.state
	 * + "</state>\n    <zip>" + this.zepCode + "</zip>\n    <country>" +
	 * this.country + "</country>\n  </address>\n  <emails>\n"; if
	 * (this.EmailAddress[0].equals(" ")) { a = a + ""; } else { for (int i = 0;
	 * i < this.EmailAddress.length; i++) { a = a + "    <email>" +
	 * this.EmailAddress[i] + "</email>\n"; } } a = a +
	 * "  </emails>\n</person>\n\n"; return a; }
	 */
	/*
	 * use this method to print the person object to the XML style
	 */
	/*
	 * public String toJson() { String a = "{\n  \"code\": \"" + this.personCode
	 * + "\",\n  \"name\": {\n    \"first\": \"" + this.firstName +
	 * "\",\n    \"last\": \"" + this.lastName +
	 * "\"\n  },\n  \"address\": {\n    \"street\": \"" + this.street +
	 * "\",\n    \"city\": \"" + this.city + "\",\n    \"state\": \"" +
	 * this.state + "\",\n    \"zip\": \"" + this.zepCode +
	 * "\",\n    \"country\": \"" + this.country + "\"\n  },"; if
	 * (this.EmailAddress[0].equals(" ")) { a = a + "\n  \"emails\": []\n}"; }
	 * else { a = a + "\n  \"emails\": ["; for (int i = 0; i <
	 * this.EmailAddress.length; i++) { a = a + "\n    \"" +
	 * this.EmailAddress[i] + "\""; if (i != this.EmailAddress.length - 1) { a =
	 * a + ","; }
	 * 
	 * } a = a + "\n  ]\n}"; } return a;
	 * 
	 * }
	 */
}
