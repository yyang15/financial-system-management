package unl.cse;

import java.util.List;

public class JuniorBroker extends Broker {
	public JuniorBroker(){
		
	}
	public JuniorBroker(String personCode, String typeOfPerson, String name,
			String address, List<String> emailAddress) {
		super(personCode, typeOfPerson, name, address, emailAddress);
		// TODO Auto-generated constructor stub
	}

	/*
	 * copy constractor
	 */

	public JuniorBroker(JuniorBroker old_juniorBroker) {
		this.EmailAddress = old_juniorBroker.EmailAddress;
		this.brokerData = old_juniorBroker.brokerData;
		this.personCode = old_juniorBroker.personCode;
		this.firstName = old_juniorBroker.firstName;
		this.lastName = old_juniorBroker.lastName;
		this.street = old_juniorBroker.street;
		this.city = old_juniorBroker.city;
		this.state = old_juniorBroker.state;
		this.country = old_juniorBroker.country;
		this.zepCode = old_juniorBroker.zepCode;
		this.typeOfPerson = old_juniorBroker.typeOfPerson;
	}
}
