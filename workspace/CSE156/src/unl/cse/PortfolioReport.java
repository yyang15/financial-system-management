package unl.cse;

//import java.io.File;
//import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

//import java.util.Scanner;

public class PortfolioReport {
	/**
	 * @param args
	 */

	public static void main(String[] args) {
		// HashMap<String, Person> personHash =
		// readPersonFile("data/Persons.dat");
		//
		// HashMap<String, Asset> assetHash = readAssetsFile("data/Assets.dat");
		//
		// Portfolios portfolios[] = readPortfilioFile("data/Portfolios.dat",
		// personHash, assetHash);

		/*
		 * this part is for printing and get the total fees, total
		 * commission,total return and total value of all portfolios.
		 */
		// PortfolioData.addPerson("yang", "y",
		// "k", "dongxin", "weinan", "shaanxi",
		// "68503", "country", "Expert","naxienian");

		HashMap<String, Person> personHash = loadPersonData();
		HashMap<String, Asset> assetHash = loadAssetsData();
		// Portfolios portfolios[] = leadPortfolioData(personHash, assetHash);
		SortedList<Portfolios> portfolios1 = loadPortfolioData(personHash,
				assetHash);
		SortedList<Portfolios> portfolios2 = new SortedList<Portfolios>(
				new CompareName());
		SortedList<Portfolios> portfolios3 = new SortedList<Portfolios>(
				new CompareType());
		for (Portfolios p : portfolios1) {
			portfolios2.add(p);
			portfolios3.add(p);
		}
//		System.out.println(portfolios1.size());
		printTitle1();
		double total_fees = 0;
		double total_com = 0;
		double total_Return = 0;
		double total_value = 0;
//
		for (Portfolios p : portfolios1) {
			p.printSummaryReport();
//			portfolio.printDetailReport();
			total_fees += p.getFees();
			total_com += p.getComissions();
			total_Return += p.getReturnValue();
			total_value += p.getTotalValue();
		}

		System.out
				.printf("\n                                                             -------------------------------------------------------------------------");
		System.out.printf("\n%40s%18s%10.2f%3s%12.2f%23s%12.2f%3s%13.2f", " ",
				"Total  $", total_fees, "$", total_com, "$", total_Return, "$",
				total_value);

//		System.out
//				.printf("\n\n\nPortfolio Details\n================================================================================================================");
//		for (Portfolios portfolio : portfolios1) {
//			portfolio.printDetailReport();
//			System.out.println(portfolio.getTotalValue()+"size");
//		}

		total_fees = 0;
		total_com = 0;
		total_Return = 0;
		total_value = 0;
		System.out.println();
		printTitle2();
		for (Portfolios portfolio : portfolios2) {
			portfolio.printSummaryReport();
			total_fees += portfolio.getFees();
			total_com += portfolio.getComissions();
			total_Return += portfolio.getReturnValue();
			total_value += portfolio.getTotalValue();
		}

		System.out
				.printf("\n                                                             -------------------------------------------------------------------------");
		System.out.printf("\n%40s%18s%10.2f%3s%12.2f%23s%12.2f%3s%13.2f", " ",
				"Total  $", total_fees, "$", total_com, "$", total_Return, "$",
				total_value);
		//
		// System.out
		// .printf("\n\n\nPortfolio Details\n================================================================================================================");
		// for (Portfolios portfolio : portfolios2) {
		// portfolio.printDetailReport();
		// }
		total_fees = 0;
		total_com = 0;
		total_Return = 0;
		total_value = 0;
		System.out.println();
		printTitle3();
		for (Portfolios portfolio : portfolios3) {
			portfolio.printSummaryReport();
			total_fees += portfolio.getFees();
			total_com += portfolio.getComissions();
			total_Return += portfolio.getReturnValue();
			total_value += portfolio.getTotalValue();
		}

		System.out
				.printf("\n                                                             -------------------------------------------------------------------------");
		System.out.printf("\n%40s%18s%10.2f%3s%12.2f%23s%12.2f%3s%13.2f", " ",
				"Total  $", total_fees, "$", total_com, "$", total_Return, "$",
				total_value);

		// System.out
		// .printf("\n\n\nPortfolio Details\n================================================================================================================");
		// for (Portfolios portfolio : portfolios3) {
		// portfolio.printDetailReport();
		// }
		// Connection conn = null;
		// conn = ConnFactory.getConnection();
		// String query =
		// "select PersonCode, BrokerData,LastName,FirstName,AddressID from Persons";
		// PreparedStatement ps = null;
		// ResultSet rs = null;
		// try {
		// ps = conn.prepareStatement(query);
		// rs = ps.executeQuery();
		// while (rs.next()) {
		// System.out.println(rs.getString("PersonCode"));
		// System.out.println(rs.getString("BrokerData") + "d");
		// }
		//
		// } catch (SQLException e) {
		// System.out.println("SQLException: ");
		// e.printStackTrace();
		// throw new RuntimeException(e);
		//
		// }
		// PortfolioData.removeAllPersons();
		// // PortfolioData.addEmail("ss", "123@gmai.com");
		// PortfolioData.removeAllAssets();
		// PortfolioData.removeAllPortfolios();
		// PortfolioData.removeAsset("123");
		// HashMap<String, Person> personHash = readPersonFile();
		// HashMap<String, Asset> assetHash = loadAssetsData();
		// System.out.println(assetHash.get("ss").getLabel());
		// readPortfilioFile(personHash, assetHash);
		// PortfolioData.addPortfolio("ss", "944c","aef1", null);
	}

	/*
	 * this method is for printing the title
	 */
	private static void printTitle1() {

		System.out.println("Portfolio Summary Report  sorted by TotalValue");

		System.out
				.println("===============================================================================================================================");
		System.out.printf("%-12s%-22s%-22s%12s%15s%20s%15s%16s", "Portfolio",
				"Owner", "Manager", "Fees", "Commisions", "Weighted Risk",
				"Return", "Total");
	}

	private static void printTitle2() {

		System.out.println("Portfolio Summary Report sorted by owner's Name");

		System.out
				.println("===============================================================================================================================");
		System.out.printf("%-12s%-22s%-22s%12s%15s%20s%15s%16s", "Portfolio",
				"Owner", "Manager", "Fees", "Commisions", "Weighted Risk",
				"Return", "Total");
	}

	private static void printTitle3() {

		System.out.println("Portfolio Summary Report  sorted by Manager");

		System.out
				.println("===============================================================================================================================");
		System.out.printf("%-12s%-22s%-22s%12s%15s%20s%15s%16s", "Portfolio",
				"Owner", "Manager", "Fees", "Commisions", "Weighted Risk",
				"Return", "Total");
	}

	/*
	 * this method is for loading person data from database
	 */
	private static HashMap<String, Person> loadPersonData() {
		HashMap<String, Person> persons_Hash = new HashMap<String, Person>();
		int AddressID = 0;
		int stateID = 0, countryID = 0, personID = 0;
		Connection conn = null;
		conn = ConnFactory.getConnection();
		String query = "select PersonID,PersonCode, BrokerData,LastName,FirstName,AddressID from Persons;";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Person p = null;
				String brokerType = rs.getString("BrokerData");
				if (brokerType == null) {
					p = new Person();
				} else if (brokerType.equals("E")) {
					p = new ExpertBroker();
				} else if (brokerType.equals("J")) {
					p = new JuniorBroker();
				} else {

				}
				p.setPersonCode(rs.getString("PersonCode"));
				p.setBrokerData(rs.getString("BrokerData"));
				personID = rs.getInt("PersonID");
				p.setLastName(rs.getString("LastName"));
				p.setFirstName(rs.getString("FirstName"));
				AddressID = rs.getInt("AddressID");
				PreparedStatement ps1 = null;
				ResultSet rs1 = null;
				query = "select Street,City,ZipCode,StateID,CountryID from Addresses where AddressID=?";
				try {
					ps1 = conn.prepareStatement(query);
					ps1.setInt(1, AddressID);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						p.setStreet(rs1.getString("Street"));
						p.setCity(rs1.getString("City"));
						p.setZepCode(rs1.getString("ZipCode"));
						stateID = rs1.getInt("StateID");
						countryID = rs1.getInt("CountryID");
					}
					query = "select State from States where StateID=?";
					try {
						ps1 = conn.prepareStatement(query);
						ps1.setInt(1, stateID);
						rs1 = ps1.executeQuery();
						if (rs1.next()) {
							p.setState(rs1.getString("State"));
						}
					} catch (SQLException e) {
						System.out.println("SQLException: ");
						e.printStackTrace();
						throw new RuntimeException(e);
					}
					query = "select Country from Countries where CountryID=?";
					try {
						ps1 = conn.prepareStatement(query);
						ps1.setInt(1, countryID);
						rs1 = ps1.executeQuery();
						if (rs1.next()) {
							p.setCountry(rs1.getString("Country"));
						}
					} catch (SQLException e) {
						System.out.println("SQLException: ");
						e.printStackTrace();
						throw new RuntimeException(e);

					}
					List<String> email = new LinkedList();
					p.setEmailAddress(email);
					query = "select EmailAddress from Emails where PersonID=?";
					try {
						ps1 = conn.prepareStatement(query);
						ps1.setInt(1, personID);
						rs1 = ps1.executeQuery();
						while (rs1.next()) {
							if (rs1.getString("EmailAddress") != null) {

								p.getEmailAddress().add(
										rs1.getString("EmailAddress"));
							}
						}

					} catch (SQLException e) {
						System.out.println("SQLException: ");
						e.printStackTrace();
						throw new RuntimeException(e);
					}
					rs1.close();
					ps1.close();

				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				persons_Hash.put(p.personCode, p);

			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return persons_Hash;
	}

	// private static HashMap<String, Person> readPersonFile(String fileName) {
	// /*
	// * This function is for reading the person.dat
	// * step 1: according each line of person.dat,
	// * build person object step 2: use hash_map map each peoson and his
	// * person code, and put all person into hash_map
	// */
	// String personCode;
	// String kindOfPerson;
	// String name;
	// String address;
	// String email;
	// Scanner s = null;
	// try {
	// s = new Scanner(new File(fileName));
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	// }
	// int nb_lines = s.nextInt();
	// String line = s.nextLine();
	// HashMap<String, Person> persons_Hash = new HashMap<String, Person>();
	// for (int i = 0; i < nb_lines; i++) {
	// line = s.nextLine();
	// String[] sub_str = line.split(";");
	//
	// personCode = sub_str[0].trim();
	// kindOfPerson = sub_str[1].trim();
	// name = sub_str[2].trim();
	// address = sub_str[3].trim();
	//
	// if (sub_str.length == 4) {
	// email = " ";
	// } else {
	// email = sub_str[4].trim();
	// }
	//
	// if (kindOfPerson.isEmpty()) {
	// persons_Hash.put(personCode, new Person(personCode,
	// kindOfPerson, name, address, email));
	// } else if (kindOfPerson.charAt(0)=='E') {
	// persons_Hash.put(personCode, new ExpertBroker(personCode,
	// kindOfPerson, name, address, email));
	// } else if (kindOfPerson.charAt(0)=='J') {
	// persons_Hash.put(personCode, new JuniorBroker(personCode,
	// kindOfPerson, name, address, email));
	// } else {
	// ;
	// }
	// }
	// return persons_Hash;
	// }
	//
	/*
	 * this method is for loading asset data from database
	 */
	private static HashMap<String, Asset> loadAssetsData() {
		HashMap<String, Asset> assets_Hash = new HashMap<String, Asset>();
		String assetCode = "";
		String label = "";
		String type = "";
		double apr = 0;
		double quarterlyDividend = 0;
		double baseRateOfRetur = 0;
		double omega = 0;
		double totalValue = 0;
		double baseRateOfReturn = 0;
		String stockSymbol = "";
		double sharePrice = 0;
		Connection conn = null;
		conn = ConnFactory.getConnection();
		String query = "select AssetCode,AssetType,Labe,Apr,QuarterlyDividend,BaseRateOfReturn,OmegaMeasure,StockSymbol,SharePrice,TotalValue from Assets";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {

				assetCode = rs.getString("AssetCode");
				label = rs.getString("Labe");
				type = rs.getString("AssetType");
				omega = rs.getDouble("OmegaMeasure");
				apr = rs.getDouble("Apr");
				quarterlyDividend = rs.getDouble("QuarterlyDividend");
				baseRateOfReturn = rs.getDouble("BaseRateOfReturn");
				stockSymbol = rs.getString("StockSymbol");
				sharePrice = rs.getDouble("SharePrice");
				totalValue = rs.getDouble("TotalValue");
				if (type.equals("S")) {
					assets_Hash.put(assetCode, new Stock(assetCode, label,
							type, quarterlyDividend, baseRateOfReturn, omega,
							stockSymbol, sharePrice));
				} else if (type.equals("P")) {
					assets_Hash.put(assetCode, new PrivateInvestment(assetCode,
							label, type, quarterlyDividend, baseRateOfReturn,
							omega, totalValue));
				} else if (type.equals("D")) {
					assets_Hash.put(assetCode, new DepositAccount(assetCode,
							label, type, apr));

				} else {
				}
				// System.out.println(new DepositAccount(assetCode,
				// label, type, apr).assetsCode);
				// System.out.println(new PrivateInvestment(assetCode,
				// label, type, quarterlyDividend, baseRateOfReturn,
				// omega, totalValue).assetsCode);
				// System.out.println(new DepositAccount(assetCode,
				// label, type, apr).assetsCode);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);

		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return assets_Hash;

	}

	// /*
	// * This function is for reading the assets.dat
	// * step 1: according each line of assets.dat,build person object
	// * step 2: use hash_map map each asset and his
	// * asset code, and put all asset into hash_map
	// */
	// private static HashMap<String, Asset> readAssetsFile(String fileName) {
	// Scanner s = null;
	// try {
	// s = new Scanner(new File(fileName));
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	// }
	//
	// double totalValue;
	//
	// int nb_lines = s.nextInt();
	// String line = s.nextLine();
	// HashMap<String, Asset> assets_Hash = new HashMap<String, Asset>();
	// for (int i = 0; i < nb_lines; i++) {
	// line = s.nextLine();
	// String[] sub_str = line.split(";");
	//
	//
	// if (sub_str[1].equals("D")) {
	// String assetsCode;
	// String label;
	// String type;
	// double apr;
	//
	// apr = Double.parseDouble(sub_str[3]) / 100.0;
	// assetsCode = sub_str[0].trim();
	// type = sub_str[1].trim();
	// label = sub_str[2].trim();
	// assets_Hash.put(assetsCode, new DepositAccount(assetsCode,
	// label, type, apr));
	// } else if (sub_str[1].equals("S")) {
	// String assetsCode;
	// String label;
	// String type;
	// double quarterlyDividend;
	// double baseRateOfReturn;
	// double omega;
	// String stockSymbol;
	// double sharePrice;
	//
	// assetsCode = sub_str[0].trim();
	// type = sub_str[1].trim();
	// label = sub_str[2].trim();
	// quarterlyDividend = Double.parseDouble(sub_str[3]);
	// baseRateOfReturn = Double.parseDouble(sub_str[4]) / 100.0;
	// omega = Double.parseDouble(sub_str[5]);
	// stockSymbol = sub_str[6].trim();
	// sharePrice = Double.parseDouble(sub_str[7]);
	// assets_Hash.put(assetsCode, new Stock(assetsCode, label, type,
	// quarterlyDividend, baseRateOfReturn, omega,
	// stockSymbol, sharePrice));
	// } else if (sub_str[1].equals("P")) {
	// String assetsCode;
	// String label;
	// String type;
	// double quarterlyDividend;
	// double baseRateOfReturn;
	// double omega;
	//
	// assetsCode = sub_str[0].trim();
	// type = sub_str[1].trim();
	// label = sub_str[2].trim();
	// quarterlyDividend = Double.parseDouble(sub_str[3]);
	// baseRateOfReturn = Double.parseDouble(sub_str[4]) / 100.0;
	// omega = Double.parseDouble(sub_str[5]);
	// totalValue = Double.parseDouble(sub_str[6]);
	// assets_Hash.put(assetsCode, new PrivateInvestment(assetsCode,
	// label, type, quarterlyDividend, baseRateOfReturn,
	// omega, totalValue));
	// }
	// }
	// return assets_Hash;
	// }
	//
	// /*
	// * This function is for reading the portfolios.dat
	// * step 1: according each line of portfolios.dat,build portfolio object
	// * step 2: put all portfolios into a Portfolios array
	// * step 3:through the person code and asset code, get the person and asset
	// */

	/*
	 * this method is for loading portfolio data from database
	 */
	private static SortedList<Portfolios> loadPortfolioData(
			HashMap<String, Person> personHash, HashMap<String, Asset> assetHash) {
		Asset result_asset;
		double tem_value = 0;
		SortedList<Portfolios> portfolioList = new SortedList<Portfolios>(
				new CompareTotalValue());
		int ownerID = 0;
		int managerID = 0;
		int beneficiaryID = 0;
		int assetID = 0;
		int portfolioID = 0;
		String portfolioCode = "";
		String ownerCode = "";
		String managerCode = "";
		String beneficiaryCode = "";
		String assetCode = "";
		List<Asset> assetList = new ArrayList<Asset>();
		Connection conn = null;
		// int numOfPortfolios = 0;
		conn = ConnFactory.getConnection();
		String query = "select count(*) from Portfolios;";
		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		Person result_owner = null;
		Broker t_broker = null;
		Broker result_broker = null;
		// try {
		// ps = conn.prepareStatement(query);
		// rs = ps.executeQuery();
		// if (rs.next()) {
		// numOfPortfolios = rs.getInt("count(*)");
		// }
		// rs.close();
		// ps.close();
		// } catch (SQLException e) {
		// System.out.println("SQLException: ");
		// e.printStackTrace();
		// throw new RuntimeException(e);
		// }
		query = "select PortfolioID,PortfolioCode,OwnerID,ManagerID,BeneficiaryID from Portfolios";
		try {
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				assetList = new LinkedList();
				portfolioID = rs.getInt("PortfolioID");
				portfolioCode = rs.getString("PortfolioCode");
				ownerID = rs.getInt("OwnerID");
				managerID = rs.getInt("ManagerID");
				beneficiaryID = rs.getInt("BeneficiaryID");
				query = "select PersonCode from Persons where PersonID=?";
				PreparedStatement ps1 = null;
				ResultSet rs1 = null;
				try {
					ps1 = conn.prepareStatement(query);
					ps1.setInt(1, ownerID);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						ownerCode = rs1.getString("PersonCode");
					}
					rs1.close();
					ps1.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				query = "select PersonCode from Persons where PersonID=?";
				try {
					ps1 = conn.prepareStatement(query);
					ps1.setInt(1, managerID);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						managerCode = rs1.getString("PersonCode");
					}
					rs1.close();
					ps1.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				query = "select PersonCode from Persons where PersonID=?";
				try {
					ps1 = conn.prepareStatement(query);
					ps1.setInt(1, beneficiaryID);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						beneficiaryCode = rs1.getString("PersonCode");
					}
					rs1.close();
					ps1.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				result_owner = new Person(personHash.get(ownerCode));
				t_broker = (Broker) personHash.get(managerCode);

				if (t_broker instanceof ExpertBroker) {
					result_broker = new ExpertBroker((ExpertBroker) t_broker);
				} else if (t_broker instanceof JuniorBroker) {
					result_broker = new JuniorBroker((JuniorBroker) t_broker);
				} else
					result_broker = null;
				// System.out.println(portfolioID+"portfolioID");
				query = "select AssetID, TemporyValue from PortfolioAssets where portfolioID=?";
				try {
					ps1 = conn.prepareStatement(query);
					ps1.setInt(1, portfolioID);
					rs1 = ps1.executeQuery();
					int num = 0;
					while (rs1.next()) {

						assetID = rs1.getInt("AssetID");
						tem_value = rs1.getDouble("TemporyValue");
						// System.out.println(assetID+"assetID");
						PreparedStatement ps2 = null;
						ResultSet rs2 = null;
						query = "select AssetCode from Assets where AssetID=?";
						try {
							ps2 = conn.prepareStatement(query);
							ps2.setInt(1, assetID);
							rs2 = ps2.executeQuery();

							if (rs2.next()) {

								assetCode = rs2.getString("AssetCode");
								Asset temp_asset = assetHash.get(assetCode);
								if (temp_asset instanceof DepositAccount) {
									result_asset = new DepositAccount(
											(DepositAccount) temp_asset);
								} else if (temp_asset instanceof Stock) {
									result_asset = new Stock((Stock) temp_asset);
								} else if (temp_asset instanceof PrivateInvestment) {
									result_asset = new PrivateInvestment(
											(PrivateInvestment) temp_asset);
								} else
									result_asset = null;
								result_asset.setTemp_value(tem_value);
								assetList.add(result_asset);
							}
							rs2.close();
							ps2.close();
						} catch (SQLException e) {
							System.out.println("SQLException: ");
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					}
					rs1.close();
					ps1.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				if (beneficiaryCode.isEmpty() && assetList.isEmpty()) {
					portfolioList.add(new Portfolios(portfolioCode,
							result_owner, result_broker, null, null));
				} else if (beneficiaryCode.isEmpty() && !assetList.isEmpty()) {
					// portfolios[count] = new Portfolios(portfolioCode,
					// result_owner, result_broker, null, assetList);
					Portfolios pl = new Portfolios(portfolioCode, result_owner,
							result_broker, null, assetList);
					// System.out.println(pl.getComissions()+"commission1");
					// System.out.println(pl.getPortfolioCode()+"PortfolioCode1");
					portfolioList.add(new Portfolios(portfolioCode,
							result_owner, result_broker, null, assetList));
				} else {
					// portfolios[count] = new Portfolios(portfolioCode,
					// result_owner, result_broker, new Person(
					// personHash.get(beneficiaryCode)), assetList);
					// System.out.println(assetList.size()+"size");
					Portfolios pp = new Portfolios(portfolioCode, result_owner,
							result_broker, new Person(
									personHash.get(beneficiaryCode)), assetList);
					// System.out.println(pp.getComissions()+"commission");
					// System.out.println(pp.getPortfolioCode()+"PortfolioCode");
					portfolioList.add(new Portfolios(portfolioCode,
							result_owner, result_broker, new Person(personHash
									.get(beneficiaryCode)), assetList));
				}

			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return portfolioList;

	}

	// private static Portfolios[] readPortfilioFile(String fileName,
	// HashMap<String, Person> personHash, HashMap<String, Asset> assetHash) {
	// Scanner s = null;
	// try {
	// s = new Scanner(new File(fileName));
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	// }
	// String portfolioCode;
	// String ownerCode;
	// String managerCode;
	// String beneficiary;
	// String assetStr;
	//
	// String line;
	// int nb_lines = s.nextInt();
	// line = s.nextLine();
	// Portfolios portfolios[] = new Portfolios[nb_lines];
	// for (int i = 0; i < nb_lines; i++) {
	// line = s.nextLine();
	// String[] sub_str = line.split(";");
	// portfolioCode = sub_str[0].trim();
	// ownerCode = sub_str[1].trim();
	// managerCode = sub_str[2].trim();
	//
	// Person result_owner = new Person(personHash.get(ownerCode));
	//
	// Broker t_broker = (Broker) personHash.get(managerCode);
	// Broker result_broker;
	//
	// if (t_broker instanceof ExpertBroker) {
	// result_broker = new ExpertBroker((ExpertBroker) t_broker);
	// } else if (t_broker instanceof JuniorBroker) {
	// result_broker = new JuniorBroker((JuniorBroker) t_broker);
	// } else
	// result_broker = null;
	//
	// if (sub_str.length == 3) {
	// portfolios[i] = new Portfolios(portfolioCode, result_owner,
	// result_broker, null, null);
	// } else if (sub_str.length == 4) {
	// beneficiary = sub_str[3].trim();
	// if (beneficiary.isEmpty()) {
	// portfolios[i] = new Portfolios(portfolioCode, result_owner,
	// result_broker, null, null);
	// } else {
	// portfolios[i] = new Portfolios(portfolioCode, result_owner,
	// result_broker, new Person(
	// personHash.get(beneficiary)), null);
	// }
	// } else {
	// beneficiary = sub_str[3].trim();
	// assetStr = sub_str[4].trim();
	// List<Asset> assetList = new ArrayList<Asset>();
	// for (String str : assetStr.split(",")) {
	// String str1[] = str.split(":");
	// Asset result_asset;
	// Asset temp_asset = assetHash.get(str1[0]);
	// if (temp_asset instanceof DepositAccount) {
	// result_asset = new DepositAccount(
	// (DepositAccount) temp_asset);
	// } else if (temp_asset instanceof Stock) {
	// result_asset = new Stock((Stock) temp_asset);
	// } else if (temp_asset instanceof PrivateInvestment) {
	// result_asset = new PrivateInvestment(
	// (PrivateInvestment) temp_asset);
	// } else
	// result_asset = null;
	//
	// result_asset.setTemp_value(Double.parseDouble(str1[1]));
	// assetList.add(result_asset);
	// }
	// if (beneficiary.isEmpty()) {
	// portfolios[i] = new Portfolios(portfolioCode, result_owner,
	// result_broker, null, assetList);
	// } else {
	// portfolios[i] = new Portfolios(portfolioCode, result_owner,
	// result_broker, new Person(
	// personHash.get(beneficiary)), assetList);
	// }
	// }
	// }
	// return portfolios;
	// }
	public static class CompareName implements Comparator<Portfolios> {
		public int compare(Portfolios o1, Portfolios o2) {
			if (o1.getOwner().getLastName()
					.compareToIgnoreCase(o2.getOwner().getLastName()) > 0)
				return 1;
			else if (o1.getOwner().getLastName()
					.compareToIgnoreCase(o2.getOwner().getLastName()) < 0)
				return -1;
			else {
				if (o1.getOwner().getFirstName()
						.compareToIgnoreCase(o2.getOwner().getFirstName()) > 0)
					return 1;
				else if (o1.getOwner().getFirstName()
						.compareToIgnoreCase(o2.getOwner().getFirstName()) < 0)
					return -1;
				else
					return 0;
			}
		}
	}

	public static class CompareTotalValue implements Comparator<Portfolios> {
		@Override
		public int compare(Portfolios tem1, Portfolios tem2) {
			if (tem1.getTotalValue() > tem2.getTotalValue()) {
				return -1;
			} else if (tem1.getTotalValue() < tem2.getTotalValue()) {
				return 1;
			} else {
				return 0;
			}

		}
	}

	public static class CompareType implements Comparator<Portfolios> {

		@Override
		public int compare(Portfolios o1, Portfolios o2) {
			if (o1.getBroker().brokerData
					.compareToIgnoreCase(o2.getBroker().brokerData) > 0)
				return 1;
			else if (o1.getBroker().brokerData.compareToIgnoreCase(o2
					.getBroker().brokerData) < 0)
				return -1;
			else {
				if (o1.getBroker().getLastName()
						.compareToIgnoreCase(o2.getBroker().getLastName()) > 0)
					return 1;
				else if (o1.getBroker().getLastName()
						.compareToIgnoreCase(o2.getBroker().getLastName()) < 0)
					return -1;
				else {
					if (o1.getBroker().getFirstName()
							.compareToIgnoreCase(o2.getBroker().getFirstName()) > 0)
						return 1;
					else if (o1.getBroker().getFirstName()
							.compareToIgnoreCase(o2.getBroker().getFirstName()) < 0)
						return -1;
					else
						return 0;
				}
			}
		}

	}

}