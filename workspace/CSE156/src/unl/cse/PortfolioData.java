package unl.cse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is a collection of utility methods that define a general API for
 * interacting with the database supporting this application.
 * 
 */
public class PortfolioData {

	/**
	 * Method that removes every person record from the database
	 */
	public static void removeAllPersons() {
		/*
		 * remove all Email records, Portfolio records and PortfolioAsset
		 * record.
		 */
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;
		String query = "delete from Emails;";
		try {
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		removeAllPortfolios();
		query = "delete from PortfolioAssets;";
		try {
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		query = "delete from Persons;";
		try {
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Removes the person record from the database corresponding to the provided
	 * <code>personCode</code>
	 * 
	 * @param personCode
	 */
	public static void removePerson(String personCode) {
		int personID = 0;
		int portfolioID = 0;
		Connection conn = null;
		conn = ConnFactory.getConnection();
		String query = "select PersonID from Persons where PersonCode=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, personCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				personID = rs.getInt("PersonID");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		query = "delete from Emails where PersonID=?";
		try {
			ps = conn.prepareStatement(query);
			ps.setInt(1, personID);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		query = "select PortfolioID from Portfolios where OwnerID=? or ManagerID=? or BeneficiaryID=?";
		try {
			ps = conn.prepareStatement(query);
			ps.setInt(1, personID);
			ps.setInt(2, personID);
			ps.setInt(3, personID);
			rs = ps.executeQuery();
			while (rs.next()) {
				portfolioID = rs.getInt("PortfolioID");
				query = "delete from PortfolioAssets where PortfolioID=?";
				ps.setInt(1, portfolioID);
				ps.executeUpdate();
			}
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		query = "delete from Portfolios where OwnerID=? or ManagerID=? or BeneficiaryID=?";
		try {
			ps = conn.prepareStatement(query);
			ps.setInt(1, personID);
			ps.setInt(2, personID);
			ps.setInt(3, personID);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		query = "delete from Persons where PersonID=?";
		try {
			ps = conn.prepareStatement(query);
			ps.setInt(1, personID);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Method to add a person record to the database with the provided data. The
	 * <code>brokerType</code> will either be "E" or "J" (Expert or Junior) or
	 * <code>null</code> if the person is not a broker.
	 * 
	 * @param personCode
	 * @param firstName
	 * @param lastName
	 * @param street
	 * @param city
	 * @param state
	 * @param zip
	 * @param country
	 * @param brokerType
	 */
	public static void addPerson(String personCode, String firstName,
			String lastName, String street, String city, String state,
			String zip, String country, String brokerType, String secBrokerId) {

		int StateID = 0;
		int CountryID = 0;
		int AddressID = 0;
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select PersonID from Persons where PersonCode=?";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, personCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("This person already exists.");
			} else {
				query = "select CountryID from Countries where Country = ?;";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, country);
					rs = ps.executeQuery();
					if (!rs.next()) {
						query = "insert into Countries(Country) value(?); ";
						try {
							ps = conn.prepareStatement(query);
							ps.setString(1, country);
							ps.executeUpdate();
							ps = conn
									.prepareStatement("select LAST_INSERT_ID()");
							rs = ps.executeQuery();
							rs.next();
							CountryID = rs.getInt("LAST_INSERT_ID()");

						} catch (SQLException e) {
							System.out.println("SQLException: ");
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					} else {
						CountryID = rs.getInt("CountryID");
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				query = "select StateID from States where State = ?;";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, state);
					rs = ps.executeQuery();
					if (!rs.next()) {
						query = "insert into States(State) value(?); ";
						try {
							ps = conn.prepareStatement(query);
							ps.setString(1, state);
							ps.executeUpdate();
							ps = conn
									.prepareStatement("select LAST_INSERT_ID()");
							rs = ps.executeQuery();
							rs.next();
							StateID = rs.getInt("LAST_INSERT_ID()");
							ps.close();
							rs.close();
						} catch (SQLException e) {
							System.out.println("SQLException: ");
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					} else {
						StateID = rs.getInt("StateID");
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				query = "select AddressID from Addresses where Street = ? and City=? and ZipCode=?;";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, street);
					ps.setString(2, city);
					ps.setString(3, zip);
					rs = ps.executeQuery();
					if (!rs.next()) {
						query = "insert into Addresses(Street,City,ZipCode,StateID,CountryID) value(?,?,?,?,?); ";
						try {
							ps = conn.prepareStatement(query);
							ps.setString(1, street);
							ps.setString(2, city);
							ps.setString(3, zip);
							ps.setInt(4, StateID);
							ps.setInt(5, CountryID);
							ps.executeUpdate();
							ps = conn
									.prepareStatement("select LAST_INSERT_ID()");
							rs = ps.executeQuery();
							rs.next();
							AddressID = rs.getInt("LAST_INSERT_ID()");
						} catch (SQLException e) {
							System.out.println("SQLException: ");
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					} else {
						AddressID = rs.getInt("AddressID");
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}

				query = "insert into Persons(PersonCode,BrokerData,Section,LastName,FirstName,AddressID) value(?,?,?,?,?,?);";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, personCode);
					ps.setString(2, brokerType);
					ps.setString(3, secBrokerId);
					ps.setString(4, lastName);
					ps.setString(5, firstName);
					ps.setInt(6, AddressID);
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Adds an email record corresponding person record corresponding to the
	 * provided <code>personCode</code>
	 * 
	 * @param personCode
	 * @param email
	 */
	public static void addEmail(String personCode, String email) {
		int personID = 0;
		Connection conn = null;
		conn = ConnFactory.getConnection();
		String query = "select PersonID from Persons where PersonCode=?;";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, personCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				personID = rs.getInt("PersonID");
				query = "insert into Emails(EmailAddress,PersonID) values(?,?);";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, email);
					ps.setInt(2, personID);
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			} else {
				System.out.println("This person is not exist");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Removes all asset records from the database
	 */
	public static void removeAllAssets() {
		Connection conn = null;
		conn = ConnFactory.getConnection();
		String query = "delete from Assets;";
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Removes the asset record from the database corresponding to the provided
	 * <code>assetCode</code>
	 * 
	 * @param assetCode
	 */
	public static void removeAsset(String assetCode) {
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select AssetID from Assets where AssetCode=?";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, assetCode);
			rs = ps.executeQuery();
			if (rs.next()) {

				query = "delete from PortfolioAssets  where AssetID=?;";

				try {
					ps = conn.prepareStatement(query);
					ps.setInt(1, rs.getInt("AssetID"));
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				query = "delete from Assets where AssetID=?";
				try {
					ps = conn.prepareStatement(query);
					ps.setInt(1, rs.getInt("AssetID"));
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			} else {
				System.out.println("this Asset is not exist!");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Adds a deposit account asset record to the database with the provided
	 * data.
	 * 
	 * @param assetCode
	 * @param label
	 * @param apr
	 */
	public static void addDepositAccount(String assetCode, String label,
			double apr) {
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select AssetID from Assets where AssetCode = ?";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, assetCode);
			rs = ps.executeQuery();
			if (!rs.next()) {
				query = "insert into Assets(AssetCode,Labe,Apr,AssetType)values(?,?,?,?);";

				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, assetCode);
					ps.setString(2, label);
					ps.setDouble(3, apr);
					ps.setString(4, "D");
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}

			} else {
				System.out.println("this Asset is alreay exist!");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Adds a private investment asset record to the database with the provided
	 * data.
	 * 
	 * @param assetCode
	 * @param label
	 * @param quarterlyDividend
	 * @param baseRateOfReturn
	 * @param omega
	 * @param totalValue
	 */
	public static void addPrivateInvestment(String assetCode, String label,
			Double quarterlyDividend, Double baseRateOfReturn, Double omega,
			Double totalValue) {
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select AssetID from Assets where AssetCode = ?";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, assetCode);
			rs = ps.executeQuery();
			if (!rs.next()) {

				query = "insert into Assets(AssetCode,Labe,QuarterlyDividend,BaseRateOfReturn,OmegaMeasure,TotalValue,AssetType)values(?,?,?,?,?,?,?);";

				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, assetCode);
					ps.setString(2, label);
					ps.setDouble(3, quarterlyDividend);
					ps.setDouble(4, baseRateOfReturn);
					ps.setDouble(5, omega);
					ps.setDouble(6, totalValue);
					ps.setString(7, "P");
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			} else {
				System.out.println("this Asset is alreay exist!");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Adds a stock asset record to the database with the provided data.
	 * 
	 * @param assetCode
	 * @param label
	 * @param quarterlyDividend
	 * @param baseRateOfReturn
	 * @param omega
	 * @param stockSymbol
	 * @param sharePrice
	 */
	public static void addStock(String assetCode, String label,
			Double quarterlyDividend, Double baseRateOfReturn, Double omega,
			String stockSymbol, Double sharePrice) {
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select AssetID from Assets where AssetCode = ?";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, assetCode);
			rs = ps.executeQuery();
			if (!rs.next()) {
				query = "insert into Assets(AssetCode,Labe,QuarterlyDividend,BaseRateOfReturn,OmegaMeasure,StockSymbol,SharePrice,AssetType)values(?,?,?,?,?,?,?,?);";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, assetCode);
					ps.setString(2, label);
					ps.setDouble(3, quarterlyDividend);
					ps.setDouble(4, baseRateOfReturn);
					ps.setDouble(5, omega);
					ps.setString(6, stockSymbol);
					ps.setDouble(7, sharePrice);
					ps.setString(8, "S");
					ps.executeUpdate();
					rs.close();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			} else {
				System.out.println("this Asset is alreay exist!");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Removes all portfolio records from the database
	 */
	public static void removeAllPortfolios() {
		/*
		 * remove all PortfolioAssets records, and Portfolio records.
		 */
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;

		String query = "delete from PortfolioAssets;";
		try {
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		query = "delete from Portfolios;";
		try {
			ps = conn.prepareStatement(query);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Removes the portfolio record from the database corresponding to the
	 * provided <code>portfolioCode</code>
	 * 
	 * @param portfolioCode
	 */
	public static void removePortfolio(String portfolioCode) {
		int PortfolioID = 0;
		Connection conn = null;
		conn = ConnFactory.getConnection();
		String query = "select PortfolioID from Portfolios where PortfolioCode=?;";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, portfolioCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				PortfolioID = rs.getInt("PortfolioID");
				query = "delete from PortfolioAssets where PortfolioID=?";
				try {
					ps = conn.prepareStatement(query);
					ps.setInt(1, PortfolioID);
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				query = "delete from Portfolios where PortfolioID=?";
				try {
					ps = conn.prepareStatement(query);
					ps.setInt(1, PortfolioID);
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			} else {
				System.out.println("This portfolio is not exist!");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Adds a portfolio records to the database with the given data. If the
	 * portfolio has no beneficiary, the <code>beneficiaryCode</code> will be
	 * <code>null</code>
	 * 
	 * @param portfolioCode
	 * @param ownerCode
	 * @param managerCode
	 * @param beneficiaryCode
	 */
	public static void addPortfolio(String portfolioCode, String ownerCode,
			String managerCode, String beneficiaryCode) {
		int ownerID = 0;
		int managerID = 0;
		Integer beneficiaryID = null;
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "select PortfolioID from Portfolios where PortfolioCode=?";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, portfolioCode);
			rs = ps.executeQuery();
			if (!rs.next()) {
				query = "select PersonID from Persons where PersonCode=?;";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, ownerCode);
					rs = ps.executeQuery();
					if (rs.next()) {
						ownerID = rs.getInt("PersonID");
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				query = "select PersonID from Persons where PersonCode=?;";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, managerCode);
					rs = ps.executeQuery();
					if (rs.next()) {
						managerID = rs.getInt("PersonID");
					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				query = "select PersonID from Persons where PersonCode=?;";
				try {
					ps = conn.prepareStatement(query);
					ps.setString(1, beneficiaryCode);
					rs = ps.executeQuery();
					if (rs.next()) {
						beneficiaryID = rs.getInt("PersonID");
					} else {

					}
					rs.close();
					ps.close();
				} catch (SQLException e) {
					System.out.println("SQLException: ");
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				if (ownerID != 0 && managerID != 0) {
					if (beneficiaryID != null) {
						query = "insert into Portfolios(PortfolioCode,OwnerID,ManagerID,BeneficiaryID) values(?,?,?,?);";
						try {
							ps = conn.prepareStatement(query);
							ps.setString(1, portfolioCode);
							ps.setInt(2, ownerID);
							ps.setInt(3, managerID);

							ps.setInt(4, beneficiaryID);

							ps.executeUpdate();
							ps.close();
						} catch (SQLException e) {
							System.out.println("SQLException: ");
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					} else {
						query = "insert into Portfolios(PortfolioCode,OwnerID,ManagerID) values(?,?,?);";
						try {
							ps = conn.prepareStatement(query);
							ps.setString(1, portfolioCode);
							ps.setInt(2, ownerID);
							ps.setInt(3, managerID);
							ps.executeUpdate();
							ps.close();
						} catch (SQLException e) {
							System.out.println("SQLException: ");
							e.printStackTrace();
							throw new RuntimeException(e);
						}
					}

				}
			} else {
				System.out.println("This portfolio is already exist!");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Associates the asset record corresponding to <code>assetCode</code> with
	 * the portfolio corresponding to the provided <code>portfolioCode</code>.
	 * The third parameter, <code>value</code> is interpreted as a
	 * <i>balance</i>, <i>number of shares</i> or <i>stake percentage</i>
	 * depending on the type of asset the <code>assetCode</code> is associated
	 * with.
	 * 
	 * @param portfolioCode
	 * @param assetCode
	 * @param value
	 */
	public static void addAsset(String portfolioCode, String assetCode,
			double value) {
		int portfolioID = 0;
		int assetID = 0;
		Connection conn = null;
		conn = ConnFactory.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select PortfolioID from Portfolios where portfolioCode=?;";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, portfolioCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				portfolioID = rs.getInt("PortfolioID");
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		query = "select AssetID from Assets where AssetCode=?;";
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, assetCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				assetID = rs.getInt("AssetID");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		if (portfolioID != 0 && assetID != 0) {
			query = "select PortfolioAssetID from PortfolioAssets where PortfolioID=? and AssetID=? and TemporyValue=?";
			try {
				ps = conn.prepareStatement(query);
				ps.setInt(1, portfolioID);
				ps.setInt(2, assetID);
				ps.setDouble(3, value);
				rs = ps.executeQuery();
				if (!rs.next()) {
					query = "insert into PortfolioAssets(PortfolioID,AssetID,TemporyValue) values(?,?,?);";
					try {
						ps = conn.prepareStatement(query);
						ps.setInt(1, portfolioID);
						ps.setInt(2, assetID);
						ps.setDouble(3, value);
						ps.executeUpdate();
					} catch (SQLException e) {
						System.out.println("SQLException: ");
						e.printStackTrace();
						throw new RuntimeException(e);
					}
				} else {
					System.out
							.println("This portfolioAssets is already exist!");
				}
				rs.close();
				ps.close();
			} catch (SQLException e) {
				System.out.println("SQLException: ");
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("SQLException: ");
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

}
