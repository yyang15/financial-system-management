package unl.cse;

public abstract class Asset {
	protected String assetsCode;
	protected String label;
	protected String type;
	protected double temp_value;

	protected double value;
	protected double returnRate;
	protected double risk;
	protected double annualReturn;

	// public abstract double getTotalValue();

	// public abstract double getRisk();

	// public abstract double getAnnualReturn();
	public Asset() {

	}

	public Asset(String assetsCode, String label, String type) {
		this.assetsCode = assetsCode;
		this.label = label;
		this.type = type;
		this.value = 0;
		this.returnRate = 0;
		this.risk = 0;
		this.annualReturn = 0;
		this.temp_value = 0;
	}

	// public abstract double compute_value();
	// public abstract double compute_return();
	// public abstract double compute_risk();
	// public abstract double compute_returnRate();
	public abstract double getValue();

	public abstract double getAnnualReturn();

	public abstract double getRisk();

	public abstract double getReturnRate();

	public double getTemp_value() {
		return temp_value;
	}

	public void setTemp_value(double temp_value) {
		this.temp_value = temp_value;
	}

	public abstract void print();

	// setter
	public void setAssetsCode(String assetsCode) {
		this.assetsCode = assetsCode;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	// getter
	public String getAssetsCode() {
		return this.assetsCode;
	}

	public String getLabel() {
		return this.label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	// public abstract void setTotalValue(Portfolios a);

	/*
	 * use this method to print the person object to the XML style
	 */
	/*
	 * public String toXML() { return "<asset> \n  <assetCode>" +
	 * this.assetsCode + "</assetCode>\n  <label>" + this.label +
	 * "</label>\n  <type>" + this.type + "</type>\n"; }
	 */
	/*
	 * use this method to print the person object to the Json style
	 */
	/*
	 * public String toJson(){ String
	 * a="{\n  \"assetCode\": \""+this.assetsCode+"\",\n  \"label\": \"";
	 * for(int i=0;i<this.label.length();i++){ if(this.label.charAt(i)=='"'){
	 * a=a+"\\"; a=a+"\""; }else{ a=a+this.label.charAt(i); } } return
	 * a+"\",\n  \"type\": \""+this.type+"\",";
	 * 
	 * }
	 */

}
