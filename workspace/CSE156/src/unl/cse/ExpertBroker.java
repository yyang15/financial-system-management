package unl.cse;

import java.util.List;


public class ExpertBroker extends Broker {
	public ExpertBroker(){
		
	}
	public ExpertBroker(String personCode, String typeOfPerson, String name,
			String address, List<String> emailAddress) {
		super(personCode, typeOfPerson, name, address, emailAddress);
		// TODO Auto-generated constructor stub
	}
	/*
	 * copy constractor 
	 */
	public ExpertBroker(ExpertBroker old_expertBroker)
	{
		this.EmailAddress = old_expertBroker.EmailAddress;
		this.brokerData = old_expertBroker.brokerData;
		this.personCode = old_expertBroker.personCode;
		this.firstName = old_expertBroker.firstName;
		this.lastName = old_expertBroker.lastName;
		this.street = old_expertBroker.street;
		this.city = old_expertBroker.city;
		this.state = old_expertBroker.state;
		this.country = old_expertBroker.country;
		this.zepCode = old_expertBroker.zepCode;
		this.typeOfPerson = old_expertBroker.typeOfPerson;
	}
}
