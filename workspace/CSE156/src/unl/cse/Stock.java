package unl.cse;

public class Stock extends Investment {

	private String stockSymbol;
	private double sharePrice;

	public Stock() {

	}

	public Stock(String assetsCode, String label, String type,
			double quarterDividend, double baseRateReturn, double omega,
			String stockSymbol, double sharePrice) {
		super(assetsCode, label, type, quarterDividend, baseRateReturn, omega);
		this.sharePrice = sharePrice;
		this.stockSymbol = stockSymbol;
		this.value = 0;
		this.returnRate = 0;
		this.risk = 0;
		this.annualReturn = 0;
		// TODO Auto-generated constructor stub
	}

	public Stock(Stock a) {
		this(a.getAssetsCode(), a.getLabel(), a.getType(), a
				.getQuarterlyDividend(), a.getBaseRateOfReturn(), a.getOmega(),
				a.getStockSymbol(), a.getSharePrice());
	}

	/*
	 * these compute-## method is for calculating.
	 */
	// public double compute_value() {
	// return this.sharePrice * this.temp_value;
	// return (double) Math.round(this.sharePrice * this.temp_value) * 100 /
	// 100;
	// }
	//
	// public double compute_risk() {
	// // TODO Auto-generated method stub
	// return this.omega + 0.10;
	// }
	//
	// @Override
	// public double compute_returnRate() {
	// // TODO Auto-generated method stub
	// return (this.baseRateOfReturn * this.sharePrice + this.quarterlyDividend
	// * 4)
	// * this.temp_value / (this.sharePrice * this.temp_value);
	// // return (double) Math
	// // .round((this.baseRateOfReturn * this.sharePrice +
	// this.quarterlyDividend * 4)
	// // * this.temp_value / (this.sharePrice * this.temp_value)) * 100 / 100;
	//
	// }
	//
	// @Override
	// public double compute_return() {
	// // TODO Auto-generated method stub
	// return (this.baseRateOfReturn * this.sharePrice + this.quarterlyDividend
	// * 4)
	// * this.temp_value;
	// }

	public void print() {
		System.out.println(this.assetsCode + "," + this.label + "," + this.type
				+ "," + this.quarterlyDividend + "," + this.baseRateOfReturn
				+ "," + this.sharePrice + "," + this.stockSymbol + ","
				+ this.omega + "," + this.temp_value);
	}

	public void assetPrint() {

	}

	public double getValue() {
		return this.sharePrice * this.temp_value;
	}

	public void setValue(double a) {
		this.value = a * this.sharePrice;
	}

	public String getStockSymbol() {
		return stockSymbol;
	}

	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}

	public double getSharePrice() {
		return sharePrice;
	}

	public void setSharePrice(double sharePrice) {
		this.sharePrice = sharePrice;
	}

	public double getRisk() {
		return this.omega + 0.10;
	}

	public void setRisk(double new_risk) {
		this.risk = new_risk;
	}

	public double getReturnRate() {
		return (this.baseRateOfReturn * this.sharePrice + this.quarterlyDividend * 4)
				* this.temp_value / (this.sharePrice * this.temp_value);
	}

	public void setReturnRate(double returnRate) {
		this.returnRate = returnRate;
	}

	public double getAnnualReturn() {
		return (this.baseRateOfReturn * this.sharePrice + this.quarterlyDividend * 4)
				* this.temp_value;
	}

	public void setAnnualReturn(double annualReturn) {
		this.annualReturn = annualReturn;
	}

	/*
	 * use this method to print the stock object to the XML style
	 */
	/*
	 * public String toXML() { return super.toXML() + "  <quarterlyDividend>" +
	 * this.quarterlyDividend + "</quarterlyDividend>\n  <baseRateOfReturn>" +
	 * this.baseRateOfReturn + "</baseRateOfReturn>\n  <omega>" + this.omega +
	 * "</omega>\n  <stockSymbol>" + this.stockSymbol +
	 * "</stockSymbol>\n  <sharePrice>" + this.sharePrice +
	 * "</sharePrice>\n</asset>\n\n"; } /* use this method to print the stock
	 * object to the Json style
	 */
	/*
	 * public String toJson(){
	 * 
	 * String a=super.toJson() + "\n  \"quarterlyDividend\": " +
	 * this.quarterlyDividend + ",\n  \"baseRateOfReturn\": " +
	 * this.baseRateOfReturn + ",\n  \"omega\": " + this.omega +
	 * ",\n  \"stockSymbol\": \""; for(int i=0;i<this.stockSymbol.length();i++){
	 * if(this.stockSymbol.charAt(i)=='"'){ a=a+"\\"; a=a+"\""; }else{
	 * a=a+this.stockSymbol.charAt(i); } } return
	 * a+"\",\n  \"sharePrice\": "+this.sharePrice+"\n}";
	 * 
	 * }
	 */

}
